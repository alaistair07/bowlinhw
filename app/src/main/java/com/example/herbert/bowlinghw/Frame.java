package com.example.herbert.bowlinghw;

/**
 * Created by herbert on 22/12/2018.
 */

import static com.example.herbert.bowlinghw.Frame.FrameType.SIMPLE;

/**
 * This class will store the state of a frame
 * A state of a frame means the pins down in both turns
 * the score for the previous frame and
 * if the current frame is a strike or a spare
 * We want to know too if this frame is over meaning
 * both turns are over.
 */
public class Frame {

    // Types of frames:
    // STRIKE: 10 pins in first turn
    // SPARE: 10 pins in second turn after failing in first turn
    // NONE: there are standing pins after two turns
    enum FrameType { SPARE, STRIKE, SIMPLE }

    FrameType frameType = FrameType.SIMPLE;

    int score = 0;

    // The number of total pins down in this frame
    int totalPinsDown = 0;

    // Pins down for the first turn
    int pinsDownFirstBall = 0;

    // Pins down for the second turn
    int pinsDownSecondBall = 0;

    // Pins down for first ball in the next frame
    // Only for spare and strike type
    private int pinsNextFrame = 0;

    // Score for a past frame, a frame behind
    int previousFrameScore = 0;

    // This state will change after setting the pins down for the first turn
    boolean isFirstTurnOver;

    // This state will change after setting the pins down for the second turn
    boolean isSecondTurnOver;

    // The score was calculated
    boolean isFinalScore;


    /**
     * This method must be call after calculating the final score of the previous frame
     * @param previousFrameScore
     */
    public void setPreviousFrameScore(int previousFrameScore) {
        this.previousFrameScore = previousFrameScore;
        if (frameType == SIMPLE && isFirstTurnOver && isSecondTurnOver){
            setFinalScore(0);
        }
    }

    public void setPinsDownFirstBall(int pinsDownFirstBall) {
        this.pinsDownFirstBall = pinsDownFirstBall;
        isFirstTurnOver = true;
        pickFrameType();
    }

    public void setPinsDownSecondBall(int pinsDownSecondBall) {
        if (!isSecondTurnOver){
            this.pinsDownSecondBall = pinsDownSecondBall;
            isSecondTurnOver = true;
            pickFrameType();
        }
    }

    /**
     * After setting the number of pins in the first or second turn
     * we should pick the type of frame
     */
    private void pickFrameType(){
        this.totalPinsDown = pinsDownFirstBall + pinsDownSecondBall;
        if (totalPinsDown == 10 && !isSecondTurnOver){
            this.frameType =  FrameType.STRIKE;
            isSecondTurnOver = true;
        } else if (totalPinsDown == 10 && isSecondTurnOver){
            this.frameType = FrameType.SPARE;
        }

    }

    /**
     * This function will be used to calculate the final score using
     * the pins down in the next frame
     * @param pinsNextFrame
     */
    public void setFinalScore(int pinsNextFrame) {
        this.pinsNextFrame = pinsNextFrame;
        if (!isFinalScore) {
            if (frameType == FrameType.STRIKE || frameType == FrameType.SPARE) {
                this.score = this.pinsNextFrame + this.previousFrameScore + 10;
            } else {
                this.score = this.totalPinsDown + this.previousFrameScore;
            }
            isFinalScore = true;
        }
    }

    /**
     * Set any score
     * @param score
     */
    public void setScore(int score) {
        this.score = score;
    }
}