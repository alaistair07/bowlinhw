package com.example.herbert.bowlinghw;

import static com.example.herbert.bowlinghw.Frame.FrameType.SIMPLE;
import static com.example.herbert.bowlinghw.Frame.FrameType.SPARE;
import static com.example.herbert.bowlinghw.Frame.FrameType.STRIKE;

/**
 * Created by herbert on 22/12/2018.
 */

public class BowlinCalculator {


    /**
     * This method will return a Frame instance with the pins down for both balls
     * In case of a strike or spare game we can send this instance to  calculate
     * the final score in the next frame which will be used for the current frame
     * @param previousFrame
     * @param pinsFirstBall
     * @param pinsSecondBall
     * @param isFirstFrame
     * @return
     * @throws Exception
     */
   static Frame calculateScore(Frame previousFrame, int pinsFirstBall, int pinsSecondBall, boolean isFirstFrame)
    throws Exception {
        Frame currentFrame = new Frame();
        currentFrame.setPinsDownFirstBall(pinsFirstBall);
        currentFrame.setPinsDownSecondBall(pinsSecondBall);
        if (isFirstFrame){
            currentFrame.setPreviousFrameScore(0);
            if (currentFrame.frameType == SIMPLE){
                currentFrame.setFinalScore(0);
            } else {
                throw new ExceptionWaitNextFrame();
            }
        } else {
            if (previousFrame != null){
                if (previousFrame.frameType == STRIKE && currentFrame.frameType != STRIKE){
                    previousFrame.setFinalScore(currentFrame.totalPinsDown);
                    currentFrame.setPreviousFrameScore(previousFrame.score);
                } else if (previousFrame.frameType == SPARE) {
                    previousFrame.setFinalScore(currentFrame.pinsDownFirstBall);
                    currentFrame.setPreviousFrameScore(previousFrame.score);
                } else if (currentFrame.frameType == STRIKE && previousFrame.frameType == STRIKE) {
                    throw new ExceptionWaitNextFrame();
                }
            } else {
                throw new ExceptionNeedPreviousFrame();
            }
        }

        return currentFrame;
    }

    /**
     * This function will be used two calculate the score of two strike frames in a row
     * It will return the current frame
     * If the current frame is a strike frame,
     * we need to call again <em>calculateScoreStrikesInRow</em> for the next frame
     * @param pastFrame
     * @param previousFrame
     * @param pinsFirstBall
     * @param pinsSecondBall
     * @return
     * @throws Exception
     */
    static Frame calculateScoreStrikesInRow(Frame pastFrame, Frame previousFrame, int pinsFirstBall, int pinsSecondBall)
            throws Exception {
        Frame currentFrame = new Frame();
        currentFrame.setPinsDownFirstBall(pinsFirstBall);
        currentFrame.setPinsDownSecondBall(pinsSecondBall);
        if (previousFrame != null || pastFrame != null){
            if (pastFrame.frameType == STRIKE && previousFrame.frameType == STRIKE && currentFrame.frameType != STRIKE){
                pastFrame.setFinalScore(previousFrame.totalPinsDown+currentFrame.pinsDownFirstBall);
                previousFrame.setPreviousFrameScore(pastFrame.score);
                currentFrame = calculateScore(previousFrame, pinsFirstBall, pinsSecondBall, false);
            } else if (pastFrame.frameType == STRIKE && previousFrame.frameType == STRIKE && currentFrame.frameType == STRIKE){
                    pastFrame.setFinalScore(previousFrame.totalPinsDown+currentFrame.pinsDownFirstBall);
                    previousFrame.setPreviousFrameScore(pastFrame.score);
                    return currentFrame;
            } else {
                throw new ExceptionNeedStrikesInRow();
            }
        } else {
            throw new ExceptionNeedPreviousFrame();
        }


        return currentFrame;
    }

    public static class ExceptionWaitNextFrame extends Exception {

    }

    public static class ExceptionNeedPreviousFrame extends Exception {

    }

    public static class ExceptionNeedStrikesInRow extends Exception {

    }

}
