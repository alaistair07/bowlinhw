package com.example.herbert.bowlinghw;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by herbert on 22/12/2018.
 */
public class BowlinCalculatorTest {

    Frame strikeFrame;
    Frame spareFrame;
    Frame simpleFrame;
    Frame currentFrame;

    @Before
    public void setUp() throws Exception {
        strikeFrame = new Frame();
        strikeFrame.setPinsDownFirstBall(10);

        spareFrame = new Frame();
        int val = new Random().nextInt(9);
        spareFrame.setPinsDownFirstBall(val);
        spareFrame.setPinsDownSecondBall(10-val);

        simpleFrame = new Frame();
        simpleFrame.setPinsDownFirstBall(3);
        simpleFrame.setPinsDownSecondBall(2);

    }

    @Test
    public void shouldCalculateFirstFrameSimple() throws Exception{
        currentFrame = BowlinCalculator.calculateScore(null, 2, 3, true);
        assertEquals(currentFrame.score, 5);
    }

    @Test
    public void shouldCalculateSpareBeforeSimple() throws Exception {
        spareFrame.setPreviousFrameScore(20);
        assertEquals(spareFrame.frameType, Frame.FrameType.SPARE);
        assertEquals(spareFrame.previousFrameScore, 20);
        currentFrame = BowlinCalculator.calculateScore(spareFrame, 2, 3, false);
        assertEquals(currentFrame.frameType, Frame.FrameType.SIMPLE);
        assertEquals(spareFrame.score, 32);
    }

    @Test
    public void shouldCalculateStrikeBeforeSimple() throws Exception {
        strikeFrame.setPreviousFrameScore(20);
        assertEquals(strikeFrame.frameType, Frame.FrameType.STRIKE);
        assertEquals(strikeFrame.previousFrameScore, 20);
        assertEquals(strikeFrame.pinsDownSecondBall, 0);
        currentFrame = BowlinCalculator.calculateScore(strikeFrame, 2, 3, false);
        assertEquals(currentFrame.frameType, Frame.FrameType.SIMPLE);
        assertEquals(strikeFrame.score, 35);
    }

    @Test
    public void shouldCalculateSpareBeforeSpare() throws Exception {
        spareFrame.setPreviousFrameScore(20);
        assertEquals(spareFrame.frameType, Frame.FrameType.SPARE);
        assertEquals(spareFrame.previousFrameScore, 20);
        currentFrame = BowlinCalculator.calculateScore(spareFrame, 3, 7, false);
        assertEquals(currentFrame.frameType, Frame.FrameType.SPARE);
        assertEquals(spareFrame.score, 33);
    }

    @Test
    public void shouldCalculateStrikeBeforeSpare() throws Exception {
        strikeFrame.setPreviousFrameScore(20);
        assertEquals(strikeFrame.frameType, Frame.FrameType.STRIKE);
        assertEquals(strikeFrame.previousFrameScore, 20);
        currentFrame = BowlinCalculator.calculateScore(strikeFrame, 3, 7, false);
        assertEquals(currentFrame.frameType, Frame.FrameType.SPARE);
        assertEquals(strikeFrame.score, 40);
    }

    @Test(expected = BowlinCalculator.ExceptionWaitNextFrame.class)
    public void shouldCalculateStrikeBeforeStrike() throws Exception {
        strikeFrame.setPreviousFrameScore(20);
        assertEquals(strikeFrame.frameType, Frame.FrameType.STRIKE);
        assertEquals(strikeFrame.previousFrameScore, 20);
        currentFrame = BowlinCalculator.calculateScore(strikeFrame, 10, 7, false);
        assertEquals(currentFrame.frameType, Frame.FrameType.SPARE);
        assertEquals(strikeFrame.score, 40);
    }

    @Test(expected = BowlinCalculator.ExceptionNeedPreviousFrame.class)
    public void shouldNotCalculateUndefinedPastFrame() throws Exception{
        currentFrame = BowlinCalculator.calculateScore(null, 2, 3, false);
    }

    @Test
    public void shouldCalculateStrikeBeforeStrikeBeforeSimple() throws Exception {
        Frame pastFrame = new Frame();
        pastFrame.setPinsDownFirstBall(10);
        pastFrame.setPreviousFrameScore(20);
        currentFrame = BowlinCalculator.calculateScoreStrikesInRow(pastFrame, strikeFrame, 3, 4);
        assertEquals(currentFrame.frameType, Frame.FrameType.SIMPLE);
        assertEquals(pastFrame.score, 43);
        assertEquals(strikeFrame.previousFrameScore, 43);
        assertEquals(strikeFrame.score, 60);
        assertTrue(strikeFrame.isFinalScore);
    }

    @Test
    public void shouldCalculateStrikeBeforeStrikeBeforeStrike() throws Exception {
        Frame pastFrame = new Frame();
        pastFrame.setPinsDownFirstBall(10);
        pastFrame.setPreviousFrameScore(30);
        currentFrame = BowlinCalculator.calculateScoreStrikesInRow(pastFrame, strikeFrame, 10, 0);
        assertEquals(currentFrame.frameType, Frame.FrameType.STRIKE);
        assertEquals(pastFrame.score, 60);
        assertEquals(strikeFrame.previousFrameScore, 60);
        assertTrue(!strikeFrame.isFinalScore);
    }

    @Test
    public void calculateRandomStrikeBeforeSimple() throws Exception {
        strikeFrame.setPreviousFrameScore(49);
        currentFrame = BowlinCalculator.calculateScore(strikeFrame, 0, 1, false);
        assertEquals(strikeFrame.score, 60);
        assertTrue(strikeFrame.isFinalScore);
        assertEquals(currentFrame.score, 61);
        assertTrue(currentFrame.isFinalScore);
    }

    @Test
    public void calculateRandomSpareBeforeSpare() throws Exception {
        spareFrame.setPreviousFrameScore(14);
        currentFrame = BowlinCalculator.calculateScore(spareFrame, 5, 5, false);
        assertEquals(spareFrame.score, 29);
        assertTrue(spareFrame.isFinalScore);
        assertTrue(!currentFrame.isFinalScore);
    }

    @Test
    public void calculateRandomSpareBeforeStrike() throws Exception {
        spareFrame.setPreviousFrameScore(29);
        currentFrame = BowlinCalculator.calculateScore(spareFrame, 10, 0, false);
        assertEquals(spareFrame.score, 49);
        assertTrue(spareFrame.isFinalScore);
        assertTrue(!currentFrame.isFinalScore);

    }



}